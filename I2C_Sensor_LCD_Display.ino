#include <OneWire.h> 
#include <DallasTemperature.h>
#include <LiquidCrystal.h>
#include "DHT.h"

const int buttonPin = 3;
const int ledPin =  12;
const int led1pin = A2;
const int led2pin = A3;
const int statpin = A1;

int buttonState = 0;        


#define DHTPIN A5
#define DHTTYPE DHT22   // DHT 22  (AM2302)
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);           

int lcd_key     = 0;
int adc_key_in  = 0;

#define ONE_WIRE_BUS A4
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);

int read_LCD_buttons(){
adc_key_in = analogRead(0);

if (adc_key_in > 1000) return btnNONE; 
if (adc_key_in < 50)   return btnRIGHT;  
if (adc_key_in < 195)  return btnUP; 
if (adc_key_in < 380)  return btnDOWN; 
if (adc_key_in < 555)  return btnLEFT; 
if (adc_key_in < 790)  return btnSELECT;   

return btnNONE;                
}

DHT dht(DHTPIN, DHTTYPE);

byte smiley[8] = {
B00000,
B10001,
B00000,
B00000,
B10001,
B01110,
B00000,
};


unsigned long lcddeb = 0;
unsigned long bouncedelay = 250;

unsigned long lastReadTime = 0;
unsigned long readTime;

float h;
float t;

int pointer;
int led1;
int led2;
int statled;

void setup(){
	lcd.createChar(0, smiley);
	pinMode(ledPin, OUTPUT);
	pinMode(led1pin, OUTPUT);
	pinMode(led2pin, OUTPUT);
	pinMode(statpin, OUTPUT);

	digitalWrite(led1pin, LOW);
	digitalWrite(led2pin, LOW); 
	digitalWrite(statpin, LOW);

	led1 = led1pin;
	led2 = led2pin;
	statled = statpin;

	pinMode(buttonPin, INPUT);

	// pinMode(relayPin, OUTPUT);
	// digitalWrite(relayPin, LOW);

	Serial.begin(9600);
	dht.begin();
	lcd.begin(16, 2);               // start the library
	lcd.write(byte(0));
	lcd.setCursor(0,0);             // set the LCD cursor   position 
	lcd.print("XXXXXXXXXXXXXXXX");  // print a simple message on the LCD
	lcd.setCursor(0,1);            // set the LCD cursor   position 
	lcd.print("XXXXXXXXXXXXXXXX"); 
	delay(1000);
	lcd.setCursor(0,0);
	lcd.print("                ");
	lcd.setCursor(0,1);
	lcd.print("                ");
	attachInterrupt(1, pin_ISR, CHANGE);
	pointer = 0;
}

void loop(){

	if (readTime - lastReadTime > 2000){


		if (isnan(h) || isnan(t)) {
			Serial.println("Failed to read from DHT sensor!");
			return;
		}

		Serial.print("H: "); 
		Serial.print(h);
		Serial.print(" %\t");
		Serial.print("TA1: "); 
		Serial.print(t);
		Serial.print(" %\t");
		Serial.print("TW1: ");
		Serial.print(sensors.getTempCByIndex(0));
		Serial.println("\r");


		h = dht.readHumidity();
		t = dht.readTemperature();
		sensors.requestTemperatures();

		statled = !statled;

		if(statled == HIGH){
		    digitalWrite(statpin, HIGH);
		}

		if(statled == LOW){
		    digitalWrite(statpin, LOW);
		}


		lastReadTime = readTime;

	}


	if(pointer == 0){

		if (readTime - lastReadTime > 2000){
			h = dht.readHumidity();
			t = dht.readTemperature();
			sensors.requestTemperatures();
			lastReadTime = readTime;
			}

		lcd.setCursor(0,0);             // set the LCD cursor   position 
		lcd.print("DHT>");
		lcd.setCursor(4,0);
		lcd.print((int) t);


		lcd.setCursor(8,0);             // set the LCD cursor   position 
		lcd.print("DHH>");
		lcd.setCursor(12,0);
		lcd.print((int) h);

		lcd.setCursor(0,1);
		lcd.print("DS18>");
		lcd.setCursor(5,1);
		lcd.print((int) sensors.getTempCByIndex(0));

		lcd.setCursor(8,1);
		lcd.print("    O..");


		lcd.setCursor(15,1);             // move to the begining of the second line
		if((millis() - lcddeb) > bouncedelay){

			lcd_key = read_LCD_buttons();   // read the buttons


			switch (lcd_key){    

				case btnRIGHT:{         
					lcd.print(">");
					if(pointer >=0){
					pointer++;
					}
					lcddeb = millis();
					break;
					}
				case btnLEFT:{
					lcd.print("<");
					lcddeb = millis();
					break;
					}    
				case btnUP:{
					lcd.print("^");

					break;
					}
				case btnDOWN:{
					lcd.print("v");
					lcddeb = millis();
					break;
					}
				case btnSELECT:{
					lcd.write(byte(0)); 
					lcddeb = millis();
					break;
					}
				case btnNONE:{
					lcd.print("_");
					break;
					}

			}
		}

		readTime = millis();
	}

	if(pointer == 1){
		lcd.setCursor(0,0);
		lcd.print(">LED1   LED2    ");

		lcd.setCursor(0,1);

		if(led1 == HIGH){
			lcd.print(" ON     ");
		}  
		else{
			lcd.print(" OFF     ");
		}

		lcd.setCursor(8,1);

		if(led2 == HIGH){
			lcd.print("ON ");
		}  
		else{
			lcd.print("OFF ");
		}

		lcd.setCursor(12,1);
		lcd.print(".O.");

		lcd.setCursor(15,1);         
		if((millis() - lcddeb) > bouncedelay){

			lcd_key = read_LCD_buttons(); 
										  
			switch (lcd_key){             
										  
				case btnRIGHT:{           
					lcd.print(">");
					pointer++; 

					lcddeb = millis();
					break;
				}
				case btnLEFT:{
					lcd.print("<");
					pointer--;
					lcddeb = millis();
					break;
				}    
				case btnUP:{
					lcd.print("^");
					lcddeb = millis();
					break;
				}
				case btnDOWN:{
					lcd.print("v");
					lcddeb = millis();
					break;
				}
				case btnSELECT:{
					lcd.write(byte(0));
					lcddeb = millis();
					led1 = !led1;
					break;
				}
				case btnNONE:{
					lcd.print("_");
					break;
				}
			}
		}
	readTime = millis();
	}

	if(pointer == 2){
		lcd.setCursor(0,0);
		lcd.print(" LED1  >LED2    ");

		lcd.setCursor(0,1);
		if(led1 == HIGH){
			lcd.print(" ON     ");
		}  
		else{
			lcd.print(" OFF     ");
		}

		lcd.setCursor(8,1);
		if(led2 == HIGH){
			lcd.print("ON ");
		}  
		else{
			lcd.print("OFF ");
		}

		lcd.setCursor(12,1);
		lcd.print(".O.");


		lcd.setCursor(15,1);            
		if((millis() - lcddeb) > bouncedelay){

			lcd_key = read_LCD_buttons(); 
			switch (lcd_key){             

				case btnRIGHT:{           
					lcd.print(">");
					pointer++;
					lcddeb = millis();
					break;
				}
				case btnLEFT:{
					lcd.print("<");
					pointer--;
					lcddeb = millis();
					break;
				}    
				case btnUP:{
					lcd.print("^"); 
					lcddeb = millis();
					break;
				}
				case btnDOWN:{
					lcd.print("v");  
					lcddeb = millis();
					break;
				}
				case btnSELECT:{
					lcd.write(byte(0));  
					lcddeb = millis();
					led2 = !led2;
					break;
				}
				case btnNONE:{
					lcd.print("_");  
					break;
				}
			}

		readTime = millis();

		}
	}

	if(pointer == 3){
		lcd.setCursor(0,0);
		lcd.print("                ");
		lcd.setCursor(0,1);
		lcd.print("                ");
		lcd.setCursor(0,0);
		lcd.print("RUNTIME>");
		lcd.setCursor(0,8);
		lcd.print(millis());

		lcd.setCursor(12,1);
		lcd.print("..O");


		lcd.setCursor(15,1);           
		if((millis() - lcddeb) > bouncedelay){

			lcd_key = read_LCD_buttons();   
			switch (lcd_key){              

				case btnRIGHT:{           
					lcd.print(">");
					lcddeb = millis();
					break;
				}
				case btnLEFT:{
					lcd.print("<"); 
					pointer--;
					lcddeb = millis();
					break;
				}    
				case btnUP:{
					lcd.print("^");  
					lcddeb = millis();
					break;
				}
				case btnDOWN:{
					lcd.print("v");  
					lcddeb = millis();
					break;
				}
				case btnSELECT:{
					lcd.write(byte(0)); 
					lcddeb = millis();
					break;
				}
				case btnNONE:{
					lcd.print("_");  
					break;
				}
			}

		}

		delay(64);

		readTime = millis();

	}

	if(led1 == HIGH){
		digitalWrite(led1pin, HIGH);
	}

	if(led1 == LOW){
		digitalWrite(led1pin, LOW);
	}

	if(led2 == HIGH){
		digitalWrite(led2pin, HIGH);
	}

	if(led2 == LOW){
		digitalWrite(led2pin, LOW);
	}

}

void pin_ISR() {
	buttonState = digitalRead(buttonPin);
	digitalWrite(ledPin, buttonState);

	if (buttonState == HIGH) {

		digitalWrite(ledPin, HIGH);
		lcd.setCursor(15,1);
		lcd.print("*");

	}
	else {
	// turn LED off:
		digitalWrite(ledPin, LOW);
		lcd.setCursor(15,1);
		lcd.print("*");
	}
}