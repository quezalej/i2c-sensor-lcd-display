# Temperature and humidity sensor + LCD display + controls

## About
A simple project that uses a LCD keypad shield to provide a user interface with temperature and humidity information, relay module control and more.

## Construction
For humidity and temperature measurement a DHT22 sensor was used. A 16x2 characters LCD shield with keypad was used in conjunction with an Arduino UNO board to provide user input and information display. For testing purposes, LEDs were used instead of a relay module.

## Sketch information
The [program](I2C_Sensor_LCD_Display.ino) allows for 3 pages of information to be displayed on screen, with a page indicator on the lower right corner. The first page displays the information that the Arduino board is constantly reading from the DHT22 sensor; the second page allows the user to turn on or off the desired LED by using the directional buttons; the third page is available to display additional information as needed (for this example, the total milliseconds of runtime are displayed). The user is able to freely scroll through the pages using the buttons, and the current status is not stored on EEPROM, meaning that powering off the device will return all the values to what is set as default in the sketch.

## Pictures
- A short video showing the device's display, as well as the navigation of multiple pages of information.

![I2C sensor+LCD shield](IMAGES/I2C-LCD.mp4)
